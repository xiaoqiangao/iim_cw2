import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Vector;
import java.util.Scanner;

public class Client {
	private Scanner userInput = new Scanner(System.in);
	String username;
	IMessage remoteObject;

	public static void main(String[] args) {
		if (args.length < 1) {
			System.out.println("Usage: ChatClient <server host name>");
			System.exit(-1);
		}
		new Client(args[0]);
	}

	public Client(String hostname) {
		Registry reg;
		try {
			// get reference to the object
			reg = LocateRegistry.getRegistry(hostname);
			// find a remote object
			remoteObject = (IMessage) reg.lookup("Server");
			loop();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
	}


	void loop() throws RemoteException {
		while (true) {
			String line;
			System.out.println("[spe]register Spectrum, [ti]register TimeHistory, [as]sort, [qu]uit: ");
			if (userInput.hasNextLine()) {
				line = userInput.nextLine();
				if (!line.matches("[staq]")) {
					System.out.println("command invalid ");
					continue;
				}
				switch (line) {
				case "spe":
					spct();
					break;
				case "ti":
					tmhs();
					break;
				case "as":
					remoteObject.printByDevice("device2");
					break;
				case "qu":
					return;
				}
			}
		}
	}


	void spct() {
        Spectrum<Integer> spectrum = new Spectrum<>("Phone", "phonering", 2021L, 123, "unit2", 0.4, 3, ScalingEnum.LINEAR);
        try {
        	remoteObject.registerSpectrum(spectrum);
        } catch (RemoteException e) {
        	e.printStackTrace();
        }
	}
	void tmhs() {
		TimeHistory<Integer> timeHistory = new TimeHistory<>("phone", "phonering", 2022L, 123, "unit1", 0.4, 3, 0.5);
		try {
			remoteObject.registerTimeHistory(timeHistory);
		} catch (RemoteException e) {
        	e.printStackTrace();
        }
	}
}