import java.rmi.*;
import java.util.Vector;

public interface Message extends Remote {
   void registerSpectrum(Spectrum Spectrum) throws RemoteException;
   void registerTimeHistory(TimeHistory TimeHistory) throws RemoteException;
   void printByDevice(String device) throws RemoteException;
};