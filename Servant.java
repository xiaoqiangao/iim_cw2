import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.stream.Collectors;
import java.util.List;
import java.util.LinkedList;

public class Servant extends UnicastRemoteObject implements IMessage {
	private List<Spectrum> spectrums = new LinkedList<>();
	private List<TimeHistory> timeHistorys = new LinkedList<>();

	public Servant() throws RemoteException {
	}

	public void registerSpectrum(Spectrum Spectrum) {
		System.out.println("Registered Spectrum");
		spectrums.add(Spectrum);
	};
	
	public void registerTimeHistory(TimeHistory TimeHistory) {
		System.out.println("Registered TimeHistory");
		timeHistorys.add(TimeHistory);
	};
	
	public void printByDevice(String device) {
		String result = spectrums.stream()
			.filter(spectrum->spectrum.device.equals(device))
			.map(Spectrum::toString)
			.collect(Collectors.joining("\n"));
		System.out.println(result);
	}
}