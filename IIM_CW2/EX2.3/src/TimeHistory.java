import java.util.Arrays;

import java.io.Serializable;//2nd

public class TimeHistory extends Sequence implements Serializable{
    private double sensitivity;

    public TimeHistory(String device, String description, long date, int channelNr,
                       String unit, double resolution, Object[] buffer, double sensitivity) {
        super(device, description, date, channelNr, unit, resolution, buffer);
        this.sensitivity = sensitivity;
    }



    public TimeHistory() {
    }

    @Override
    public String toString() {
        return "TimeHistory{" +
                "device='" + device + '\'' +
                ", description='" + description + '\'' +
                ", date=" + date +
                ", channelNr=" + channelNr +
                ", unit='" + unit + '\'' +
                ", resolution=" + resolution +
                ", buffer=" + Arrays.toString(buffer) +
                ", sensitivity=" + sensitivity +
                '}';
    }
}
