import javax.tools.Tool;
import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UDPClient {
    public static void main(String[] args) {

        DatagramSocket aSocket = null;

        try {
            // args contain server hostname
            if (args.length < 1) {
                System.out.println("Usage: UDPClient <server host name>");
                System.exit(-1);
            }
            //1st
            TimeHistory timeHistory = new TimeHistory("Phone", "RingRing", 132L, 12, "LINEAR", 12.2, new Integer[2], 12.3);
            //Spectrum spectrum = new Spectrum();

            //2nd
            byte[] data = Tools.serialize(timeHistory);
            byte[] buffer = new byte[1024];
            InetAddress aHost = InetAddress.getByName(args[0]);
            int serverPort = 9876;
            aSocket = new DatagramSocket();
            DatagramPacket request = new DatagramPacket(data, data.length, aHost, serverPort);
            aSocket.send(request);
        } catch (SocketException ex) {
            Logger.getLogger(UDPClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnknownHostException ex) {
            Logger.getLogger(UDPClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UDPClient.class.getName()).log(Level.SEVERE, null, ex);
        } //catch (ClassNotFoundException e) {
          //  e.printStackTrace();}
        finally {
            aSocket.close();
        }
    }
}