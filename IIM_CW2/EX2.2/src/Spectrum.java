import java.util.Arrays;

public class Spectrum extends Sequence{
    Scaling scaling;

    public Spectrum(String device, String description, long date, int channelNr,
                    String unit, double resolution, Object[] buffer, Scaling scaling) {
        super(device, description, date, channelNr, unit, resolution, buffer);
        this.scaling = scaling;
    }

    public Spectrum() {
    }

    @Override
    public String toString() {
        return "Spectrum{" +
                "device='" + device + '\'' +
                ", description='" + description + '\'' +
                ", date=" + date +
                ", channelNr=" + channelNr +
                ", unit='" + unit + '\'' +
                ", resolution=" + resolution +
                ", buffer=" + Arrays.toString(buffer) +
                ", scaling=" + scaling +
                '}';
    }
}
