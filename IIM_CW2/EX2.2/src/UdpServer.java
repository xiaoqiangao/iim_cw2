import javax.tools.Tool;
import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UdpServer {
    public static void main(String[] args) {
        DatagramSocket aSocket = null;
        try {
            // args contain message content and server hostname
            aSocket = new DatagramSocket(9876);
            byte[] buffer = new byte[1024];
            while (true) {
                DatagramPacket request = new DatagramPacket(buffer, buffer.length);
                System.out.println("Waiting for request...");
                aSocket.receive(request);

                Packet read = (Packet) Tools.deserialize(buffer);


                System.out.println("Received: " + read);

                DatagramPacket reply = new DatagramPacket(request.getData(), request.getLength(),
                        request.getAddress(), request.getPort());
                aSocket.send(reply);
                String path = read.device + read.date;
                Files.write(new File(read.device + read.date).toPath(), reply.getData());


            }
        } catch (SocketException ex) {
            Logger.getLogger(UdpServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UdpServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            aSocket.close();
        }

    }
}

