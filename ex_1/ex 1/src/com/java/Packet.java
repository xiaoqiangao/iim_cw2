package com.java;

public abstract class Packet {
    protected String device;
    protected String description;
    protected long date;


    public Packet() {
    }

    public Packet(String device, String description, long date) {
        this.device = device;
        this.description = description;
        this.date = date;
    }


    @Override
    public String toString() {
        return "Packet{" +
                "device='" + device + '\'' +
                ", description='" + description + '\'' +
                ", date=" + date +
                '}';
    }
}
