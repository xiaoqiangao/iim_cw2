package com.java;

public enum Scaling {
    LINEAR,
    LOGARITHMIC;

    Scaling() {
    }
}
