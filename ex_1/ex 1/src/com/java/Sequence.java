package com.java;

import java.util.Arrays;

public abstract class Sequence<T> extends Packet{
    protected int channelNr;
    protected String unit;
    protected double resolution;
    protected T[] buffer;

    public Sequence(String device, String description, long date, int channelNr, String unit, double resolution, T[] buffer) {
        super(device, description, date);
        this.channelNr = channelNr;
        this.unit = unit;
        this.resolution = resolution;
        this.buffer = buffer;
    }

    public Sequence() {
    }

    @Override
    public String toString() {
        return "Sequence{" +
                "device='" + device + '\'' +
                ", description='" + description + '\'' +
                ", date=" + date +
                ", channelNr=" + channelNr +
                ", unit='" + unit + '\'' +
                ", resolution=" + resolution +
                ", buffer=" + Arrays.toString(buffer) +
                '}';
    }
}
