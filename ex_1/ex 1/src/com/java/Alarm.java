package com.java;

public class Alarm extends Packet{
    private int channelNr;
    private int threshold;
    private Direction direction;

    public Alarm(String device, String description, long date,
                 int channelNr, int threshold, Direction direction) {
        super(device, description, date);
        this.channelNr = channelNr;
        this.threshold = threshold;
        this.direction = direction;
    }

    public Alarm() {
    }

    @Override
    public String toString() {
        return "Alarm{" +
                "channelNr=" + channelNr +
                ", threshold=" + threshold +
                ", direction=" + direction +
                ", device='" + device + '\'' +
                ", description='" + description + '\'' +
                ", date=" + date +
                '}';
    }
}
